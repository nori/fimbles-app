package is.fimbles.fimbles;

import is.fimbles.fimbles.data.CreateResponse;
import is.fimbles.fimbles.data.Game;
import is.fimbles.fimbles.data.GameList;
import is.fimbles.fimbles.data.Player;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @GET("game/api/{id}/")
    Call<Game> game(@Path("id") int gameId);

    @GET("api/me")
    Call<Player> me();

    @GET("game/api/gamelist/")
    Call<GameList> gamelist();

    @POST("game/api/create")
    Call<CreateResponse> createGame();

    @POST("game/{id}/join/")
    Call<ResponseBody> join(@Path("id") int gameId);

    @POST("game/{id}/leave/")
    Call<ResponseBody> leave(@Path("id") int gameId);

    @FormUrlEncoded
    @POST("game/api/{id}/explain/")
    Call<ResponseBody> explain(@Path("id") int gameId, @Field("explanation") String explanation);

    @FormUrlEncoded
    @POST("signup")
    Call<ResponseBody> signup(@Field("email") String email, @Field("username") String username, @Field("password") String password, @Field("passwordRepeated") String passwordRepeated);

    @FormUrlEncoded
    @POST("game/api/{id}/vote/")
    Call<ResponseBody> vote(@Path("id") int gameId, @Field("explanationId") int explanationId);
}
