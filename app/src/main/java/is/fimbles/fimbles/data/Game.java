package is.fimbles.fimbles.data;

import java.util.List;

public class Game {
    public int playerExplanationId;
    public int currentRound;
    public List<Slot> slots;
    public List<String> explanations;
    public String correctExplanation;
    public String state;
    public String word;
    public List<Integer> correctExplanationVotedBy; // TODO: check if this is actually ints, not strings
    public long timestamp;
}
