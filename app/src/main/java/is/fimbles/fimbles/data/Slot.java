package is.fimbles.fimbles.data;

import java.util.List;

/**
 * Created by arnor on 07/02/16.
 */
public class Slot {
    public String submission;
    public List<String> votedBy;
    public int id;
    public int points;
    public String username;
}
