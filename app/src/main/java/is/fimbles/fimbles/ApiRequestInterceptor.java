package is.fimbles.fimbles;

import android.util.Base64;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiRequestInterceptor implements Interceptor {
    private final String username;
    private final String password;

    public ApiRequestInterceptor(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest = request;
        if (username != null && password != null) {
            final String authorizationValue = encodeCredentialsForBasicAuthorization();
            newRequest = request.newBuilder().addHeader("Authorization", authorizationValue).build();
        }
        return chain.proceed(newRequest);
    }

    private String encodeCredentialsForBasicAuthorization() {
        final String userAndPassword = username + ":" + password;
        return "Basic " + Base64.encodeToString(userAndPassword.getBytes(), Base64.NO_WRAP);
    }
}
