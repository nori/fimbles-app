package is.fimbles.fimbles;

import okhttp3.OkHttpClient;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by arnor on 08/02/16.
 */
public class ApiHelper {
    public static ApiService getApiService(String username, String password) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ApiRequestInterceptor(username, password))
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://fimbles.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(ApiService.class);
    }
    public static ApiService getApiService() {
        OkHttpClient client = new OkHttpClient.Builder()
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://fimbles.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(ApiService.class);
    }
}
