package is.fimbles.fimbles.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import is.fimbles.fimbles.ApiHelper;
import is.fimbles.fimbles.ApiService;
import is.fimbles.fimbles.GamelistAdapter;
import is.fimbles.fimbles.GamelistJoinListener;
import is.fimbles.fimbles.R;
import is.fimbles.fimbles.data.CreateResponse;
import is.fimbles.fimbles.data.GameList;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements GamelistJoinListener {

    private GamelistAdapter adapter;
    private SharedPreferences settings;
    private Handler handler;
    private Runnable updateRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        settings = getSharedPreferences("UserInfo", 0);
        String username = settings.getString("username", "");
        if (username.length() > 0) {
            // Is logged in

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.gamelist);
            recyclerView.setHasFixedSize(true);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new GamelistAdapter(this);
            recyclerView.setAdapter(adapter);
            Log.d("LOGINCHECK", "message: logged in");
            updateGameList();
            handler = new Handler();
            updateRunnable = new Runnable() {
                @Override
                public void run() {
                    updateGameList();
                    handler.postDelayed(this, 5000);
                }
            };
        } else {
            Log.d("LOGINCHECK", "message: not logged in");
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(updateRunnable, 500);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacksAndMessages(null);
    }

    private void updateGameList() {
        ApiService apiService = getApiService();
        apiService.gamelist().enqueue(new Callback<GameList>() {
            @Override
            public void onResponse(Response<GameList> response) {
                GameList list = response.body();
                if(list==null){
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
                Log.d("ZZZ", "got games, list:" + list);
                Log.d("ZZZ", "got games, games:" + list.games);
                Log.d("ZZZ", "got games, count:" + list.games.size());
                adapter.setGameList(list);
                adapter.notifyDataSetChanged();
                TextView nogames = (TextView) findViewById(R.id.nogames);
                RecyclerView gamelist = (RecyclerView) findViewById(R.id.gamelist);
                if (list.games.size() == 0) {
                    nogames.setVisibility(View.VISIBLE);
                    gamelist.setVisibility(View.GONE);
                }
                else{
                    nogames.setVisibility(View.GONE);
                    gamelist.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Failed to get gamelist", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        } else if (id == R.id.action_refresh) {
            updateGameList();
        } else if (id == R.id.action_new_game) {
            Log.d("ZZZ", "new game");
            createGame();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createGame() {
        ApiService apiService = getApiService();
        apiService.createGame().enqueue(new Callback<CreateResponse>() {
            @Override
            public void onResponse(Response<CreateResponse> response) {
                if (response.body().error != null) {
                    Toast.makeText(MainActivity.this, response.body().error, Toast.LENGTH_LONG).show();
                } else {
                    joinGame(response.body().gameId);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Failed to create game", Toast.LENGTH_LONG).show();
            }
        });
    }

    private ApiService getApiService()  {
        return ApiHelper.getApiService(settings.getString("email", ""), settings.getString("password", ""));
    }

    private void logout() {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("email", "");
        editor.putString("username", "");
        editor.putString("password", "");
        editor.apply();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void joinGame(int id) {
        Intent intent = new Intent(MainActivity.this, GameActivity.class);
        intent.putExtra(GameActivity.GAME_ID, id);
        startActivity(intent);
    }
}
