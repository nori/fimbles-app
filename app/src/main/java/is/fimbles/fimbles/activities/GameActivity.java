package is.fimbles.fimbles.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import is.fimbles.fimbles.ApiHelper;
import is.fimbles.fimbles.ApiService;
import is.fimbles.fimbles.R;
import is.fimbles.fimbles.data.Game;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class GameActivity extends AppCompatActivity {
    public static final String GAME_ID = "gameId";
    private static final int UPDATE_INTERVAL = 1000;

    private int gameId;
    private SharedPreferences settings;
    private TextView wordInfoBox;
    private TextView stateBox;
    private TextView slotBox;
    private EditText explanationInput;
    private Handler handler;
    private ApiService apiService;

    private String lastState = "";
    private String myExplanation;
    private int timePassed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handler = new Handler();
        settings = getSharedPreferences("UserInfo", 0);
        Intent intent = getIntent();
        gameId = intent.getIntExtra(GAME_ID, -1);

        getSupportActionBar().setTitle("Game " + gameId);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        wordInfoBox = (TextView) findViewById(R.id.word_info_box);
        stateBox = (TextView) findViewById(R.id.state_status_box);
        wordInfoBox.setText(R.string.joining_game);
        slotBox = (TextView) findViewById(R.id.slot_info_box);

        apiService = ApiHelper.getApiService(settings.getString("email", ""), settings.getString("password", ""));
        apiService.join(gameId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                wordInfoBox.setText(R.string.loading_game);
                updateGameStatus();
                scheduleGameStatusUpdate();
            }

            @Override
            public void onFailure(Throwable t) {
                wordInfoBox.setText(R.string.failed_join);
            }
        });

        Button explainButton = (Button) findViewById(R.id.submit_explanation_button);

        explainButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                explanationInput = (EditText) findViewById(R.id.explanation_box);
                final String explanation = explanationInput.getText().toString();
                myExplanation = explanation.trim();
                Log.d("Button", "Explanation: " + explanation);

                apiService.explain(gameId, explanation).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response) {
                        wordInfoBox.setText(R.string.explanation_submitted);
                        ((Button) findViewById(R.id.submit_explanation_button)).setVisibility(View.GONE);
                        explanationInput.setVisibility(View.GONE);
                        explanationInput.setText("");
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(explanationInput.getWindowToken(), 0);
                    }
                    @Override
                    public void onFailure(Throwable t) {
                        wordInfoBox.setText(R.string.failed_to_submit_explanation);
                    }
                });
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        apiService.leave(gameId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                Log.d("ZZZ", "left game");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("ZZZ", "failed to leave game");
            }
        });
    }

    private void updateGameStatus() {
        apiService.game(gameId).enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Response<Game> response) {
                Game game = response.body();

                if (game.slots == null) {
                    wordInfoBox.setText(R.string.failed_to_load_game);
                    return;
                }

                getSupportActionBar().setTitle("Game " + gameId + " - Round: " + game.currentRound);
                wordInfoBox.setText("Word: " + game.word);
                String slots = "";
                for (int i = 0; i < game.slots.size(); i++) {
                    slots += "User: " + game.slots.get(i).username +
                            " Points: " + game.slots.get(i).points + "\n";
                }
                slotBox.setText(slots);

                Log.d("ZZZ", game.state);

                timePassed = timePassed + UPDATE_INTERVAL;

                switch (game.state) {
                    case "WRITE_EXPLANATION":
                        if(!lastState.equals(game.state)) {
                            timePassed = 0;
                        }
                        writeExplanation();
                        break;
                    case "WAITING_FOR_OTHER_EXPLANATIONS":
                        waitForExplanations();
                        break;
                    case "VOTE_EXPLANATION":
                        if(!lastState.equals(game.state)) {
                            timePassed = 0;
                        }
                        voteExplanation(lastState, game);
                        break;
                    case "WAITING_FOR_OTHER_VOTES":
                        waitForVotes(lastState, game);
                        break;
                    case "SHOW_RESULTS":
                        if(!lastState.equals(game.state)) {
                            timePassed = 0;
                        }
                        showResults(lastState, game);
                        break;
                }

                int maxSeconds = 60;
                if (game.state.equals("SHOW_RESULTS")) {
                    maxSeconds = 10;
                }

                lastState = game.state;
                ProgressBar timer = (ProgressBar) findViewById(R.id.timer);
                long timeLeft = game.timestamp - (new Date()).getTime() + maxSeconds*1000;
                timeLeft = Math.max(timeLeft, 0);
                int seconds = (int) timeLeft/1000;
                timer.setProgress((int) (100.0*seconds/maxSeconds));

                TextView timerText = (TextView) findViewById(R.id.timer_text);
                timerText.setText("Time left: " + seconds + " seconds");

                timer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                wordInfoBox.setText(R.string.failed_to_load_game);
            }
        });
    }

    private void writeExplanation() {
        stateBox = (TextView) findViewById(R.id.state_status_box);
        stateBox.setText("");

        ((Button) findViewById(R.id.submit_explanation_button)).setVisibility(View.VISIBLE);
        ((EditText) findViewById(R.id.explanation_box)).setVisibility(View.VISIBLE);
        LinearLayout explanationLayout = (LinearLayout) findViewById(R.id.explanations);
        explanationLayout.setVisibility(View.GONE);
        explanationLayout.removeAllViews();
    }

    private void waitForExplanations() {
        stateBox = (TextView) findViewById(R.id.state_status_box);
        stateBox.setText(R.string.waiting_for_other_explanations);
    }

    private void voteExplanation(String lastState, Game game) {
        stateBox = (TextView) findViewById(R.id.state_status_box);
        stateBox.setText("");
        String currentState = game.state;
        List<String> explanations = game.explanations;
        LinearLayout explanationLayout = (LinearLayout) findViewById(R.id.explanations);
        explanationLayout.setVisibility(View.VISIBLE);
        if(!lastState.equals(currentState)) {
            for(int i = 0; i<explanations.size(); i++) {
                TextView tv = createNewTextView(explanations.get(i), i);
                if(i == game.playerExplanationId) {
                    tv.setBackgroundResource(R.drawable.own_explanation);
                } else {
                    tv.setBackgroundResource(R.drawable.vote_button);
                }
                tv.setTag(explanations.get(i));
                explanationLayout.addView(tv);
            }
        }
    }

    private void waitForVotes(String lastState, Game game) {
        stateBox = (TextView) findViewById(R.id.state_status_box);
        stateBox.setText(R.string.waiting_for_other_votes);

        String currentState = game.state;
        List<String> explanations = game.explanations;
        LinearLayout explanationLayout = (LinearLayout) findViewById(R.id.explanations);
        if(!lastState.equals(currentState)) {
            for(int i = 0; i<explanations.size(); i++) {
                TextView tv = (TextView)explanationLayout.findViewWithTag(explanations.get(i));
                if (tv != null) {
                    tv.setBackgroundResource(R.drawable.disabled_vote);
                    tv.setOnClickListener(null);
                }
            }
        }
    }

    private void showResults(String lastState, Game game) {
        stateBox = (TextView) findViewById(R.id.state_status_box);
        stateBox.setText(R.string.results);

        String currentState = game.state;
        List<String> explanations = game.explanations;
        LinearLayout explanationLayout = (LinearLayout) findViewById(R.id.explanations);
        if(!lastState.equals(currentState)) {
            for(int i = 0; i<explanations.size(); i++) {
                TextView tv = (TextView)explanationLayout.findViewWithTag(explanations.get(i));
                if (tv != null) {
                    if(explanations.get(i).equals(game.correctExplanation)) {
                        tv.setBackgroundResource(R.drawable.correct_explanation);
                    } else {
                        tv.setBackgroundResource(R.drawable.disabled_vote);
                    }
                    tv.setOnClickListener(null);
                }
            }
        }
    }

    private TextView createNewTextView(String text, final int index) {
        final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParams.setMargins(calcPixels(3), calcPixels(5), calcPixels(3), calcPixels(5) );
        final TextView textView = new TextView(this);
        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String textClicked = ((String) textView.getText()).trim();
                if(textClicked.equals(myExplanation)) {
                    Toast.makeText(
                            GameActivity.this,
                            "Can't vote your own explanation",
                            Toast.LENGTH_LONG
                    ).show();
                }
                else {
                    Toast.makeText(
                            GameActivity.this,
                            "Voted: " + textClicked,
                            Toast.LENGTH_LONG
                    ).show();
                }

                apiService.vote(gameId, index).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response) {
                        Log.d("YYY", "success!");
                    }
                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("YYY", "failed :(");
                    }
                });
            }
        });
        textView.setLayoutParams(lParams);
        textView.setText(text);
        return textView;
    }


    private void scheduleGameStatusUpdate() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateGameStatus();
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(this, UPDATE_INTERVAL);
            }
        }, UPDATE_INTERVAL);
    }


    private int calcPixels(int dp) {
        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        Log.d("PX", "" +px);
        return px;
    }
}
