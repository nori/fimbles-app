package is.fimbles.fimbles.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import is.fimbles.fimbles.ApiHelper;
import is.fimbles.fimbles.ApiService;
import is.fimbles.fimbles.R;
import is.fimbles.fimbles.data.GameList;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText username = (EditText) findViewById(R.id.username);
        final EditText password = (EditText) findViewById(R.id.password);
        final EditText repeat_password = (EditText) findViewById(R.id.repeat);
        final TextView error_message = (TextView) findViewById(R.id.error_message);
        Button create_user = (Button) findViewById(R.id.create_user);
        final ApiService apiService = ApiHelper.getApiService();

        create_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error_message.setText("");
                boolean error = false;
                if(!repeat_password.getText().toString().equals(password.getText().toString())){
                    error = true;
                    password.setError("Passwords don't match");
                }
                if(username.getText().toString().length() < 5){
                    error = true;
                    username.setError("Username must be at least 5 characters long");
                }
                if(!error){
                    Toast.makeText(SignupActivity.this,"Should create user",Toast.LENGTH_LONG).show();
                    apiService.signup(email.getText().toString(),username.getText().toString(), password.getText().toString(),repeat_password.getText().toString()).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Response<ResponseBody> response) {
                            String responseBody = "";
                            try {
                                Log.d("ZZZ",response.body().string()+"");
                                responseBody = response.body().string()+"";

                            } catch (IOException e) {
                                Log.d("ZZZ", "virka[i ekki");
                            }

                            if(responseBody.indexOf("Sign up") != -1){
                                email.setError("email is taken");
                            }
                            else {
                                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("email", email.getText().toString());
                                editor.putString("username", username.getText().toString());
                                editor.putString("password", password.getText().toString());
                                editor.apply();
                                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            error_message.setText("Error");
                        }
                    });
                }
            }
        });
    }
}
