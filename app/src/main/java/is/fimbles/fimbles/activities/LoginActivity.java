package is.fimbles.fimbles.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import is.fimbles.fimbles.ApiHelper;
import is.fimbles.fimbles.ApiService;
import is.fimbles.fimbles.R;
import is.fimbles.fimbles.data.Player;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText emailField = (EditText) findViewById(R.id.email);
        final EditText passwordField = (EditText) findViewById(R.id.password);
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;
                if (emailField.getText().length() == 0) {
                    emailField.setError("Please enter a username");
                    error = true;
                }

                if (passwordField.getText().length() == 0) {
                    passwordField.setError("Please enter a password");
                    error = true;
                }

                if (!error) {
                    final String email = emailField.getText().toString();
                    final String password = passwordField.getText().toString();
                    ApiService service = ApiHelper.getApiService(email, password);
                    service.me().enqueue(new Callback<Player>() {
                        @Override
                        public void onResponse(Response<Player> response) {
                            Player player = response.body();
                            if (player == null) {
                                Toast.makeText(LoginActivity.this, "Wrong username or password", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(LoginActivity.this, "Welcome " + player.username, Toast.LENGTH_LONG).show();
                                SharedPreferences settings = getSharedPreferences("UserInfo", 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("email", email);
                                editor.putString("username", player.username);
                                editor.putString("password", password);
                                editor.apply();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Toast.makeText(LoginActivity.this, "Network failure", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        Button signupButton = (Button) findViewById(R.id.signup_button);
        signupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
    }
}
