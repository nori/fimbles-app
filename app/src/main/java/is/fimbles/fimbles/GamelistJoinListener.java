package is.fimbles.fimbles;

/**
 * Created by arnor on 2/14/16.
 */
public interface GamelistJoinListener {
    void joinGame(int id);
}
