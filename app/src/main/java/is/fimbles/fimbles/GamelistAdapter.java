package is.fimbles.fimbles;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import is.fimbles.fimbles.data.GameList;

public class GamelistAdapter extends RecyclerView.Adapter<GamelistAdapter.ViewHolder> {
    private final GamelistJoinListener listener;
    private GameList gameList;

    public GamelistAdapter(GamelistJoinListener listener) {
        this.listener = listener;
    }

    public void setGameList(GameList gameList) {
        Log.d("ZZZ", "setGameList");
        this.gameList = gameList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView gameName;
        public TextView slotCount;
        public TextView joinGame;
        public TextView currentRound;

        public ViewHolder(View v) {
            super(v);
            Log.d("ZZZ", "ViewHolder");
            gameName = (TextView) v.findViewById(R.id.game_name);
            slotCount = (TextView) v.findViewById(R.id.slot_count);
            joinGame = (TextView) v.findViewById(R.id.join_game_button);
            currentRound = (TextView) v.findViewById(R.id.current_round);
        }
    }

    @Override
    public GamelistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("ZZZ", "onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gamelist_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final GamelistAdapter.ViewHolder holder, int position) {
        Log.d("ZZZ", "onBindViewHolder");
        holder.gameName.setText("Game " + gameList.games.get(position).id);
        holder.slotCount.setText("(" + gameList.games.get(position).slots.size() + "/6)");
        holder.joinGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = gameList.games.get(holder.getAdapterPosition()).id;
                listener.joinGame(id);
            }
        });
        holder.currentRound.setText(String.format(Locale.US, "Round %d", gameList.games.get(position).currentRound));
    }

    @Override
    public int getItemCount() {
        Log.d("ZZZ", "getItemCount");
        if (gameList == null) return 0;
        return gameList.games.size();
    }
}
